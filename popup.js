document.addEventListener('DOMContentLoaded', function() {
  $.ajax({
    method: "GET",
    url: "http://fiftycoupons.herokuapp.com/goldbox-deals"
  }).done(function( responseJSON ) {
    $("#deal-list-element-loading-box").hide();
    addDealElementsToList(responseJSON);
  });
});

function addDealElementsToList(listOfDealEntities){
  var deals_list = document.getElementById("deal-list-ul-id");

  for(i=0;i<listOfDealEntities.length;i++){
    var newLi = getDealListElement(listOfDealEntities[i]);
    deals_list.appendChild(newLi);
  }
}

function getDealListElement (elementAttributes) {

  var newListElement = document.createElement("li");

  var newDivElement = document.createElement("div");
  newDivElement.className="deal-list-element-div";

  var productImage = document.createElement("img");
  var productHeadingURL = document.createElement("h3");
  var productPriceDetails = document.createElement("p");

  productImage.src = elementAttributes[d_img_url];
  productHeadingURL.innerHTML = buildProductHeadingURLInnerHTML(elementAttributes);
  productPriceDetails.innerHTML = buildProductPriceDetailsInnerHTML(elementAttributes);

  newDivElement.appendChild(productImage);
  newDivElement.appendChild(productHeadingURL);
  newDivElement.appendChild(productPriceDetails);

  newListElement.appendChild(newDivElement);

  return newListElement;
}

function buildProductHeadingURLInnerHTML(eA){
  var startTag = "<a href='"+ eA[d_product_url] +"' target='blank' class='simple_url'>";
  var endTag = "</a>";
  var headingURL = startTag + eA[d_title] + endTag;
  return headingURL;
}

function buildProductPriceDetailsInnerHTML(eA){

  var priceNode =      "<strong>Price:</strong> ";
  var oldPriceNode =   "<strike>" + eA[d_original_price] + "</strike> ";
  var dealPriceNode =  "<span class='discount_price_span'>"+eA[d_discounted_price]+"</span>";
  //var discountNode =   " (" + eA[d_discount] + " Discount )";
  //var ratingNode =     "<br/><strong> Rating: </strong>" + eA[d_rating] + "/5";

  var consolidatedListOfNodes = [priceNode,oldPriceNode,dealPriceNode];
  return consolidatedListOfNodes.join("");
}
