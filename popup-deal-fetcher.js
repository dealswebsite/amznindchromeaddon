function getListOfDealEntities(){
  var elementAttribs =
  {
    dealTitle:"Fastrack Sunglasses",
    originalPrice:"Rs 1200",
    dealPrice:"Rs 1080",
    d_discount:"10%",
    d_claimed_percentage:"60%",
    d_ends_in_time:"D Ends In Time",
    d_rating:"4.001",
    d_product_url:"D url",
    dealURL:"D category",
    imageURL:"https://images-eu.ssl-images-amazon.com/images/I/31j7g2hyRtL._AC_UL260_SR200,260_FMwebp_QL70_.jpg"
  };
  var elementAttribs2 =
  {
    dealTitle:"Logitech Bluetooth adapter",
    originalPrice:"Rs 1000",
    dealPrice:"Rs 900",
    d_discount:"10%",
    d_claimed_percentage:"60%",
    d_ends_in_time:"D Ends In Time",
    d_rating:"4.001",
    d_product_url:"D url",
    d_deal_category:"D category",
    dealURL:"url",
    imageURL:"http://ecx.images-amazon.com/images/I/31YwghAeNnL._SX425_.jpg"
  };
  var elementAttribs3 =
  {
    dealTitle:"Fastrack Sunglasses",
    originalPrice:"Rs 1000",
    dealPrice:"Rs 900",
    d_discount:"10%",
    d_claimed_percentage:"60%",
    d_ends_in_time:"D Ends In Time",
    d_rating:"4.001",
    d_product_url:"D url",
    d_deal_category:"D category",
    dealURL:"url",
    imageURL:"https://images-eu.ssl-images-amazon.com/images/I/31j7g2hyRtL._AC_UL260_SR200,260_FMwebp_QL70_.jpg"
  };
  //return [elementAttribs,elementAttribs2,elementAttribs];
  return sampleJSON;
}
